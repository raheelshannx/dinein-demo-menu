const initialState = [
  {
    name: "English",
    short_name: "en",
    isDefault: true,
    type: "LTR",
    symbol: "EN",
  },
];

const languageItem = (state, action) => {
  switch (action.type) {
      case 'SET_LANGUAGE':
          return {
              ...state,
              isDefault: !state.isDefault
          }
      default:
          return state;
  }
}

export default function languageReducer(state = initialState, action) {
  switch (action.type) {
    case "ADD_LANGUAGE":
      let item = state.find(
        (item) => item.short_name == action.payload.short_name
      );
      if (item == undefined) {
        return [...state, action.payload];
      } else {
        return state;
      }
    case "SET_LANGUAGE":
      return state.map(t => languageItem(t, action));
    default:
      return state;
  }
}
