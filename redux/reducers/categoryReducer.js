const categoryItem = (state, action) => {
    switch (action.type) {
        case 'ADD_CATEGORY':
            return action.payload;
        default:
            return state;
    }
}

export default function categoryReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_CATEGORY':
            let item = state.find(item =>  item.id == action.payload.id);
            if (item == undefined) {
                return [
                    ...state,
                    categoryItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}