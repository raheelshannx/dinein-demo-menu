import React from "react";
import { useSelector } from "react-redux";

function Title() {
  const store = useSelector((state) => state.store);

  return (
    <div className="text-center mt-lg-4 mt-2 title">
      <div className="brand">
        <img src={store.logo} loading="lazy" />
      </div>
      <h5>{store.name}</h5>
    </div>
  );
}
export default Title;
