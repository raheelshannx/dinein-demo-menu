import React from 'react';

function FeedBackModal() {
  return <div className="modal messageModal fade" id="exampleModalCenter" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div className="modal-dialog modal-dialog-centered" role="document">
      <div className="modal-content">
        <div className="modal-body">
          <h3>
            {" "}
            <a className="back" href="#" title="index" data-dismiss="modal">
              <img src="assets/img/back.png" />
            </a>{" "}
            Feedback
          </h3>
          <form className="feedbackController">
            <p>Is this your first time at our restaurant?</p>
            <button role="button">Yes</button>
            <button role="button">No</button>
            <p>What is your overall satisfaction with our restaurant?</p>
            <span>
              <img src="assets/img/stars.png" alt="rating" />
            </span>
            <p>how would you rate the hygiene?</p>
            <span>
              <img src="assets/img/stars.png" alt="rating" />
            </span>
            <p>How would rate the taste of our food</p>
            <span>
              <img src="assets/img/stars.png" alt="rating" />
            </span>
            <p>Would you come back to eat with us again?</p>
            <button role="button">Yes</button>
            <button role="button">No</button>
            <p>Is there anything else you want to tell us?</p>
            <input type="text" className="form-content" />
            <p>Name</p>
            <input type="text" className="form-content" />
            <p>Email</p>
            <input type="text" className="form-content" />
          </form>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-block btn-secondary" data-dismiss="modal">
            t
          </button>
        </div>
      </div>
    </div>
  </div>;
}
export default FeedBackModal;