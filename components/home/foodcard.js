import React from "react";
import CategoryItem from "./categoryitem";
import Tab from "./tab";
import { useSelector } from "react-redux";

function Foodcard() {
  const categories = useSelector((state) => state.categories);
  const products = useSelector((state) => state.products);

  return (
    <div className="food_card" id="tabs">
      <ul className="nav nav-tabs" role="tablist">
        {categories.map((category, index) => {
          return (
            <CategoryItem key={category.en} data={category} index={index} />
          );
        })}
      </ul>

      <div className="tab-content">
        {categories.map((category, index) => {
          let prods = products.filter((p) => {
            if (p.category_id == category.id) {
              return p;
            }
          });
          return (
            <Tab
              category={category}
              products={prods}
              key={category.id}
              index={index}
            />
          );
        })}
      </div>
    </div>
  );
}
export default Foodcard;
