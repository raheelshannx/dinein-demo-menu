import React from 'react';
import Head from "next/head";

function Script() {
    return (
        <div>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"  integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossOrigin="anonymous"></script>
            <script src="assets/js/bootstrap.bundle.js"  ></script>
            <script src="assets/js/main.js"  ></script>
        </div>
    );
}

export default Script;