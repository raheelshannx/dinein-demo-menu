import Head from "next/head";

import ContentMenu from "./contentmenu";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";

export default function Detail() {
  const [product, setProduct] = useState(null);
  const products = useSelector((state) => state.products);
  const filteredProducts = products.filter((prod) => prod.view);
  const filteredProduct = filteredProducts[0];

  useEffect(() => {
    if (filteredProduct) {
      setProduct(filteredProduct);
    }
  }, [filteredProduct]);

  const renderSection = () => {
    if (product == null) {
      return <div className="loader">Loding...</div>;
    } else {
      return (
        <main role="main" className="main_wrapper">
          <ContentMenu product={product} />
        </main>
      );
    }
  };

  return (
    <div
      className="modal messageModal fade"
      id="detailModal"
      tabIndex={-1}
      role="dialog"
      aria-labelledby="detailModalTitle"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">{renderSection()}</div>
      </div>
    </div>
  );
}
