import React from "react";
import { useSelector } from "react-redux";

function ProductCardItem({ product, id }) {
  const languages = useSelector((state) => state.language);
  const filtered = languages.filter((item) => item.isDefault);
  const lang = filtered[0];

  return (
    <li role="presentation">
      <a
        href="#"
        aria-controls="home"
        className={product.product_id == id ? "active" : ""}
      >
        <img
          src={product.image}
          alt={product[lang.short_name].name}
          className="product-item"
          loading="lazy"
        />
        <h3>{product[lang.short_name].name}</h3>
      </a>
    </li>
  );
}

export default ProductCardItem;
