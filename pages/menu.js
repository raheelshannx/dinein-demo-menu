import Head from "next/head";

import ContentFood from "../components/home/contentfood";
import FeedBackModal from "../components/home/feedbackmodal";
import Footer from "../components/home/footer";
import Theme from "../components/home/theme";

export default function Menu() {

  const renderData = () => {
    return (
      <div>
        <main role="main" className="main_wrapper">
          <div className="contentMenuWrapper">
            <ContentFood />
          </div>
        </main>
        {/* <FeedBackModal /> */}

        <Footer />
        <Theme />
      </div>
    );
  };

  return <>{renderData()}</>;
}
