import Head from "next/head";

import ContentFood from "../components/home/contentfood";
import FeedBackModal from "../components/home/feedbackmodal";
import Footer from "../components/home/footer";
import Theme from "../components/home/theme";
import useFetch from "../service/useFetch";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addLanguage } from "../redux/actions/languageAction";
import { addProduct } from "../redux/actions/productAction";
import { addCategory } from "../redux/actions/categoryAction";
import { setStore } from "../redux/actions/storeAction";
import { useRouter } from "next/router";
import Link from "next/link";
import axios from "axios";

export default function Home() {
  const dispatch = useDispatch();
  const router = useRouter();
  const { store_id } = router.query;
  const url = `https://services.nextaxe.com/api/outlets?store_id=${store_id}`;
  const [response, loading, hasError] = useFetch(url, store_id);

  const [branch, setBranch] = useState("");
  const [branches, setBranches] = useState([]);
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");

  useEffect(() => {
    if (response != null && response != undefined && Array.isArray(response)) {
      setBranches(response);
    }
  });
  const onSelectBranch = (event) => {
    let url = `https://services.nextaxe.com/api/menu?store_id=${store_id}&outlet_id=${event.target.value}`;
    setBranch(event.target.value);

    axios.get(url).then((resp) => {
      const response = resp.data;
      if (response != null || response != undefined) {
        if (response.products.length > 0) {
          response.products.map((product) => {
            dispatch(addProduct(product));
          });
        }
        if (response.categories.length > 0) {
          response.categories.map((category) => {
            dispatch(addCategory(category));
          });
        }
        if (response.languages.length > 0) {
          response.languages.map((language, index) => {
            language.isDefault = index == 0 ? true : false;
            dispatch(addLanguage(language));
          });
        }
        if (response.store != null) {
          dispatch(setStore(response.store));
        }
      }
    });
  };

  const onNameChange = (event) => {
    setName(event.target.value);
  };
  const onPhoneChange = (event) => {
    setPhone(event.target.value);
  };
  const handleSubmit = (event) => {
    if (branch == 0) {
      alert("Please select a Branch.");
      event.preventDefault();
    }
  };
  const renderData = (response) => {
    const url = `/menu?store_id=${store_id}&outlet_id=${branch}`;
    return (
      <div className="card">
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            <div className="form-group">
              <label>SELECT BRANCH</label>
              <select
                onChange={onSelectBranch}
                value={branch}
                required
                className="form-control"
              >
                <option style={dropDownItem} value={0}>
                  Select Branch
                </option>
                {branches.map((branch) => {
                  return (
                    <option
                      style={dropDownItem}
                      key={branch.id.toString()}
                      value={branch.id}
                    >
                      {branch.name}
                    </option>
                  );
                })}
              </select>
            </div>
          </li>
          <li className="list-group-item">
            <div className="form-group">
              <label>NAME</label>
              <input
                className="form-control"
                type="text"
                value={name}
                onChange={onNameChange}
                placeholder="Your Name"
              />
            </div>
          </li>
          <li className="list-group-item">
            <div className="form-group">
              <label>PHONE NUMBER</label>
              <input
                className="form-control"
                type="number"
                value={phone}
                onChange={onPhoneChange}
                placeholder="Phone No."
              />
            </div>
          </li>
        </ul>
        <div className="card-body">
          <Link href={url}>
            <button className="btn btn-secondary" onClick={handleSubmit}>
              GO
            </button>
          </Link>
        </div>
      </div>
    );
  };
  const dropDown = {
    height: "35px",
  };
  const dropDownItem = {
    fontSize: "17px",
  };
  const labelStyle = {};
  const inputStyle = {
    border: "none",
    borderBottom: "1px solid",
    height: "35px",
  };
  const btnStyle = {
    position: "fixed",
    bottom: 60,
    right: 60,
    backgroundColor: null,
  };
  //   return (
  //     <>
  //       {loading ? (
  //         <div className="loader">Loading...</div>
  //       ) : hasError ? (
  //         <div className="loader">
  //           Error occured. Please refresh the page to try again
  //         </div>
  //       ) : (
  //         renderData(response)
  //       )}
  //     </>
  //   );

  return <>{renderData(response)}</>;
}
