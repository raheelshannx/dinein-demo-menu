import { useState, useEffect } from "react";

function useLanguage() {
  const [language, setLanguage] = useState('en');

  const fetchData = async () => {
    const lang = localStorage.getItem("default_language");
    setLanguage(lang)
  };

  useEffect(() => {
    fetchData();
  }, []);
  return [language];
}

export default useLanguage;
