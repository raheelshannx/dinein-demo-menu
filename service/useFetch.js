import React, { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";

let initialState = {
  languages: [],
  products: [],
  categories: [],
  store: {},
};

function useFetch(url,store_id) {
  const [response, setResponse] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const products = useSelector((state) => state.products);

  
  
  const fetchData = async () => {
    try {
      setLoading(true);

      await axios.get(url).then((response) => {
        setLoading(false);
        setResponse(response.data);
      });
    } catch (error) {
      console.error(error);
      setHasError(true);
      setLoading(false);
    }
  };

  useEffect(() => {
    if(products.length == 0){
      if(store_id > 0){
        fetchData();
      }
    }
  }, [store_id]);
  return [response, loading, hasError];
}

export default useFetch;