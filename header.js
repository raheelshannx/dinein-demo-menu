import Head from "next/head";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";

export default function Header() {
  const [sheet, setSheet] = useState("");
  const languages = useSelector((state) => state.language);
  const store = useSelector((state) => state.store);
  const filtered = languages.filter((item) => item.isDefault);
  let dynamicSheet = `assets/css/${
    filtered[0].type == "RTL" ? "theme_green" : "theme_red"
  }.css`;

  const titleH2 = {
    color: "black",
  };
  const titleH5 = {
    color: "black",
  };
  const navTabsLiHoverH3 = {
    color: "black",
    borderBottom: "black",
  };
  const navTabsLiAActiveH3 = {
    color: "black",
    borderBottom: "black",
  };
  const navTabsLiH3 = {
    color: "black",
  };
  const cardFoodDetailsH3 = {
    color: "black",
  };
  const messageModalButton = {
    background: "black",
  };
  const itemDetailH3 = {
    color: "black",
  };

  useEffect(() => {
    setSheet(dynamicSheet);
  }, [dynamicSheet]);

  return (
    <Head>
      <meta charSet="utf-8" />
      <title>Dine Menu : Food | Order Food</title>
      <meta content="public" httpEquiv="Cache-control" />
      <meta content="width=device-width, user-scalable=no" name="viewport" />
      <meta content="Dine Menu, food menu, food " />
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href={store.favicon}
      ></link>
      <meta
        content="Dine Menu, food menu, food online food food order"
        name="keywords"
      />
      <link href="https://dinemenu.com/" rel="canonical" />
      <link href="favicon.png" rel="icon" />
      <link href="assets/css/bootstrap.css" rel="stylesheet" />
      <link href="assets/css/style.css" rel="stylesheet" />
      {/* Theme Color */}
      <link href={sheet} rel="stylesheet" ref="pagestyle" id="pagestyle" />
    </Head>
  );
}
